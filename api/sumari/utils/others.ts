"use strict";

const compose =
  (...fns: any) =>
  (x: any) =>
    fns.reduceRight((y: any, f: any) => f(y), x);

export default compose;
