"use strict";
import compose from "./others";

const allMinuscule = (text: string): string => text.toLowerCase();

const cleanWords = (text: string): RegExpMatchArray | null =>
  text.match(/[\wäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ\u00f1\u00d1]+/g);

const sentences = (text: string): RegExpMatchArray | null =>
  text.match(
    /[ A-Za-zäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ-\u00f1\u00d1:"()–-]+/g
  );

const addDictionaryValues = (value: object) =>
  Object.values(value).reduce((acc, el) => acc + el);

const halfOfText = (sum: number) => (Dictionary: object) =>
  sum / Object.keys(Dictionary).length;

const filterValues = (value: string) => (list: object) =>
  Object.entries(list).filter((x) => x[0] == value);

const thereIsWord = (sentence: string) => (Dictionary: object) =>
  Object.keys(Dictionary).indexOf(sentence);

const summarizeText = (half: number) => (Dictionary: any) => {
  let text: string = "";
  Object.keys(Dictionary).forEach((sentence: string) => {
    if (Dictionary[sentence] > 1.4 * half) {
      text += " " + sentence;
    }
  });
  return text;
};

const wordList = (text: []) =>
  text.reduce((acc: any, el: string) => {
    let minuscule = el.toLowerCase();
    if (acc[minuscule]) acc[minuscule]++;
    else acc[minuscule] = 1;
    return acc;
  }, {});

const iterateWords =
  (sentence: string) => (list: object) => (Dictionary: any) =>
    Object.keys(list).forEach((word) => {
      if (sentence.includes(word)) {
        let sum = filterValues(word)(list);
        let check = thereIsWord(sentence)(Dictionary);
        if (check > -1) Dictionary[sentence] += sum[0][1];
        else Dictionary[sentence] = sum[0][1];
      }
    });

const iterateSentences = (sentences: []) => (list: object) => {
  let Dictionary = {};
  sentences.forEach((sentence) => {
    iterateWords(sentence)(list)(Dictionary);
  });

  return Dictionary;
};

const cleanText = compose(cleanWords, allMinuscule);

const cleanSentences = compose(sentences, allMinuscule);

export = {
  cleanText,
  cleanSentences,
  iterateSentences,
  wordList,
  addDictionaryValues,
  halfOfText,
  summarizeText,
};
