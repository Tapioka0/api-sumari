"use strict";
import sumari from "./utils/summarize";

const scriptResumen = (text: string) => {
  const clearText = sumari.cleanText(text);

  const clearSentences = sumari.cleanSentences(text);

  let list = sumari.wordList(clearText);

  let Dictionary = sumari.iterateSentences(clearSentences)(list);
  let sumaFinal = sumari.addDictionaryValues(Dictionary);
  let half = sumari.halfOfText(sumaFinal)(Dictionary);
  return sumari.summarizeText(half)(Dictionary);
};

export default scriptResumen;
