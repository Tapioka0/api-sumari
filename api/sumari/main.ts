import scriptResumen from "./scriptSumari";

const runSummarize = (text: string) => {
  return scriptResumen(text);
};

export default runSummarize;
