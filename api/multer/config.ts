import multer from "multer";
const storage = multer.diskStorage({
  destination: function (
    req: Express.Request,
    file: Express.Multer.File,
    callback: (error: Error | null, destination: string) => void
  ) {
    callback(null, "./api/uploads/");
  },
  filename: function (
    req: Express.Request,
    file: Express.Multer.File,
    callback: (error: Error | null, filename: string) => void
  ) {
    callback(null, new Date().toISOString() + "_" + file.originalname);
  },
});

const upload = multer({
  storage: storage,
});

export default upload;
