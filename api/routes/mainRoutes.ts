import express from "express";
import { Request, Response } from "express";
import sumarizeRouter from "./simpleSumari";

const router = express.Router();

router
  .route("/status")

  .get((req: Request, res: Response) => {
    res.send("Ok");
  });

router.use("/sumari", sumarizeRouter);

export default router;
