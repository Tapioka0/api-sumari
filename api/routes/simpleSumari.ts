import express from "express";
import { resumir } from "../Controllers/sumari";
import upload from "../multer/config";
import { Readertxt } from "../Controllers/sumariFileTxt";
import { ReaderDocx } from "../Controllers/readDocx";
import { ReaderPDF } from "../Controllers/readPDF";
const router = express.Router();

router.route("/").post(resumir);
router.route("/txt").post(upload.single("txt"), Readertxt);
router.route("/pdf").post(upload.single("pdf"), ReaderPDF);
router.route("/docx").post(upload.single("docx"), ReaderDocx);

export default router;
