// @ts-ignore
import pdf from "pdf-parse";

import mammoth from "mammoth";
import { Request, Response } from "express";
import runSummarize from "../sumari/main";
import fs from "fs";

export const ReaderPDF = async (req: Request, res: Response): Promise<void> => {
  let path: string | undefined = req.file?.filename;
  if (path && path.endsWith(".pdf")) {
    let dataBuffer = fs.readFileSync(`../uploads/${path}`);
    let data = await pdf(dataBuffer);
    console.log(data);
    res.status(200).send(runSummarize(data.text));
    fs.rmSync(`../uploads/${path}`);
  } else res.status(400);
};
