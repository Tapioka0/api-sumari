import mammoth from "mammoth";
import { Request, Response } from "express";
import runSummarize from "../sumari/main";
import fs from "fs";

export const ReaderDocx = async (
  req: Request,
  res: Response
): Promise<void> => {
  let path: string | undefined = req.file?.filename;
  if (path && path.endsWith(".docx")) {
    let text = await mammoth.extractRawText({ path: `../uploads/${path}` });

    res.status(200).send(runSummarize(text.value));
    fs.rmSync(`../uploads/${path}`);
  } else res.status(400).send("no");
};
