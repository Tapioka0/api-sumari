import { Request, Response } from "express";
import runSummarize from "../sumari/main";

export interface TypedRequestBody<T> extends Request {
  body: T;
}

export const resumir = async (
  req: TypedRequestBody<{ texto: string; password: string }>,
  res: Response
): Promise<void> => {
  let texto = req.body.texto;

  if (typeof texto === "string") res.status(200).send(runSummarize(texto));

  res.status(400).send();
};
