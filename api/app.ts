import express from "express";
import Cors from "cors";
import routes from "./routes/mainRoutes";
import { Request, Response } from "express";
import hola from "./uploads/noDelete"
import fs from "fs"
console.log(hola)
//initialize app
const app = express();
app.use(express.static(__dirname + "/static"));
// Setting port to enviroment variable PORT or 3001
app.set("port", 3007);

//Middlewares
// application/json
app.use(express.json());
// CORS Policy

app.use(Cors());
app.get("/favicon.ico", (req, res) => {
  console.log("enviado");
  res.status(200).sendFile("./static/favicon.ico");
});
//Setting routes
app.get("/", (req: Request, res: Response) => {

const buscarArchivos = (path: string) =>
  fs.readdirSync(path, { withFileTypes: true });
let si: any = [];
buscarArchivos("./api").map((x: any) => {
  console.log(x);
});

res.send("Hello World")
})
  
app.use("/api/v1", routes);
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.status(404).send("404 ¿Buscando paginas secretas?");
});

// starting the server
app.listen(app.get("port"), () =>
  console.log(`Server listen on port ${app.get("port")}`)
);

export default app;
