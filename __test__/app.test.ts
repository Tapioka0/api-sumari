import app from "../api/app";
import request from "supertest";

describe("GET / - a simple api endpoint", () => {
  it("Hello API Request", async () => {
    const result = await request(app).post("/api/v1/sumari");
    let esperado =
      "       konrad zuse propuso la idea de los primeros idiomas de alto nivel  cuando se creó el primer lenguaje de programación de alto nivel para computadoras electrónicas       el código corto fue el primer lenguaje de programación funcional       un programa tiene que ser traducido al código de la máquina cada vez que se ejecuta  autocode fue desarrollado por alick glennie       fue el primer lenguaje compilado que se convierte directamente en código máquina usando un compilador       fortran fue el primer lenguaje de programación popular que fue desarrollado en        es el lenguaje de programación de alto nivel más antiguo que aún se utiliza  todavía había tiempo para que salieran los lenguajes de alto nivel como java       java y muchos otros lenguajes de alto nivel están basados de alguna manera en algol";

    expect(result.text).toEqual(esperado);
    expect(result.statusCode).toEqual(200);
  });
});
